

function cardPressed() {
    this.classList.add('card-hover');
}

function cardReleased() {
    this.classList.remove('card-hover');
}

function hamburgerMenuPressed() {
    if (this.parentNode.classList.contains('hamburger-menu-open')) {
        document.body.classList.remove('no-scroll');
        this.parentNode.classList.remove('hamburger-menu-open')
        this.setAttribute('aria-expanded', "false");
        document.body.style.paddingRight = 0 + "px";
    } else {
        document.body.style.paddingRight = window.innerWidth - document.documentElement.clientWidth + "px";
        document.body.classList.add('no-scroll');
        this.parentNode.classList.add('hamburger-menu-open')
        this.setAttribute('aria-expanded', "true");
    }

}

$('[data-fancybox]').fancybox({
    protect: true,
    buttons: [
        "zoom",
        // "share",
        "slideShow",
        // "fullScreen",
        //"download",
        "thumbs",
        "close"
    ],
});

// GDPR or cookie consent for the web
// ----------------------------------
window.cookieconsent.initialise({
    "palette": {
        "popup": {
            "background": "#222222",
            "text": "#ffffff"
        },
        "button": {
            "background": "#bb3333"
        }
    },
    "position": "bottom-right",
    "content": {
        "message": "Website ini menggunakan cookies untuk memastikan pengalaman terbaik dalam berselancar.",
        "dismiss": "OK!",
        "link": "Lebih lanjut"
    }
});