---
title: "{{ replace .TranslationBaseName "-" " " | title }}"
date: {{ .Date }}
description: ""
dropCap: true
displayInMenu: false
displayInList: true
draft: false
---
