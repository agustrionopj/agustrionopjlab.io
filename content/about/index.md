+++
title = "Profil"
date = "2020-02-24"
displayInMenu = true
displayInList = false
dropCap = true
+++

{{<raw>}}
    <img class="crop" src="/img/profpic2.jpg">
{{</raw>}}

{{<raw>}}
<div class="rata-tengah">
    <a href="https://www.facebook.com/agus.pj" target="_blank" class="fa fa-facebook"></a>
    <a href="https://twitter.com/agustrionopj" target="_blank" class="fa fa-twitter"></a>
    <a href="https://www.instagram.com/agustrionopj" target="_blank" class="fa fa-instagram"></a>
</div>
{{</raw>}}

Teman-temanku memanggilku dengan `PJ`. Saya adalah penyuka Astronomi, fotografi dan musik. Waktu senggang saya gunakan untuk bermain piano, _coding_, menenteng kamera dan memotret apapun, serta sesekali bermain game di laptop. 

Dari sisi pekerjaan, minat penelitian saya cukup luas. Pekerjaan saya saat ini adalah **mengembangkan teknik observasi dan analisis data fenomena okultasi dan bintang variabel**. Saya mengembangkan sebuah _code_ menggunakan `Python` dan `PyMC3` untuk mereduksi dan menganalisis kurva cahaya okultasi bintang oleh Bulan dan obyek lain seperti asteroid dan planet.

Silakan cek beberapa publikasi yang pernah saya keluarkan pada tautan  {{<raw>}}<a href="https://bosscha.itb.ac.id/id/publication/" target="_blank">berikut</a>.{{</raw>}}