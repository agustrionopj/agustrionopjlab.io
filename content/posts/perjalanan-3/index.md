---
title: "Perjalanan (Part 3)"
date: 2019-02-17T22:49:20+07:00
description: "Catatan perjalanan ke Eropa. (3/3)"
categories: ["Journey"]
dropCap: true
displayInMenu: false
displayInList: true
draft: false

resources:
    - name: featuredImage
      src: "journey.jpg"
---

Tulisan ini adalah bagian ke-3 sekaligus bagian terakhir dari rangkaian tulisan 'Perjalanan'. Silakan baca bagian  {{<raw>}}<a href="/posts/perjalanan-1/" target="_blank" > pertama </a>{{</raw>}} dan  {{<raw>}}<a href="/posts/perjalanan-2/" target="_blank" > kedua </a>{{</raw>}}, jika anda ingin mengikuti cerita ini dari awal.

### Den Haag, Belanda

Oh iya, ada yang terlupa saya ceritakan. Di sela-sela rangkaian _workshop_, saya memutuskan untuk pergi ke Den Haag (the Hague). Niat saya adalah mencari suasana baru, sekaligus untuk bertemu seorang rekan yang lain yang tinggal di Den Haag bersama suaminya. Kehadiran saya di Den Haag disambut oleh hujan yang cukup deras. Setelah bertemu dengan sang suami dan putranya yang datang menjemput saya di seputaran Den Haag centraal, maka kami menuju ke sebuah restoran Indonesia, di mana rekan saya menunggu di sana. Kami berempat makan malam sambil bertukar kabar dan bernostalgia. Kami juga menuju ke sebuah toko tanaman yang letaknya (ternyata) cukup jauh dari restoran tersebut, untuk membeli titipan teman berupa bibit tanaman. Sayangnya, tanaman yang dicari sedang tidak tersedia sehingga kamipun kembali ke area centruum dan berpisah di sana. Kamipun tidak sempat foto bersama karena saya harus mengejar kereta ke Leiden dari Den Haag. Saya pun kembali ke Leiden malam itu juga dan sampai di hotel sekitar pukul 23:30.

{{<raw>}}
<p class="gallery" style="max-width: 1000px;">
  <a href="1/denhaag1.jpg" data-fancybox="images" data-caption="Masjid di Den Haag, tempat kami menunaikan shalat maghrib">
    <img src="1/denhaag1.jpg" />
  </a>

  <a href="1/denhaag2.jpg" data-fancybox="images" data-caption="Daerah pecinan di Den Haag">
    <img src="1/denhaag2.jpg" />
  </a>
</p>
{{</raw>}}

### Leiden, Belanda (hari kepulangan)

Pada hari terakhir, saya memanfaatkan waktu untuk kembali berjalan-jalan di pusat kota. Setelah _check out_ dan menitipkan barang, saya bergegas menuju ke centruum. Kali ini saya ditemani oleh tiga rekan: dua orang dari Indonesia (yaitu teman saya yang kuliah di sana dan partnernya (*ehm*) serta teman dari Yunani. Kami berempat berkeliling pusat kota, pergi ke pasar, membeli cemilan ikan goreng (entah apa namanya, namun enak sekali), serta duduk-duduk di taman. Cuaca kurang begitu bersahabat, sehingga kami memutuskan untuk menyudahi kegiatan kami. Saya tiba kembali di hotel sekitar pukul 14:00 dan segera berpamitan dengan resepsionis hotel yang ramah. Singkat cerita, saya kembali menaiki kereta dari Leiden centraal ke Schiphol, _check in_ dan menembus imigrasi, boarding serta akhirnya _take off_ tepat pukul 19:15.

{{<foldergallery src="foto">}}

### Istanbul, Turki - Kuala Lumpur, Malaysia

Saya tiba di Istanbul sekitar pukul 23:50 waktu lokal. Ternyata, penerbangan lanjutan saya berikutnya mengalami penundaan. Hal ini membuat saya khawatir karena penerbangan saya dari KL ke Jakarta sangat mepet waktunya dengan tibanya penerbangan saya dari Istanbul ke KL. Meskipun akhirnya penerbangan saya ke KL berhasil berangkat, saya menghabiskan malam itu dengan perasaan tidak tenang. Pesawat yang saya naiki sampai di KL dengan jadwal yang terlambat dari yang seharusnya. Dengan terburu-buru saya kembali menembus imigrasi dan mengambil bagasi serta menuju ke gerbang keberangkatan pesawat lanjutan \\(\ldots\\) dan saya pun terlambat, gerbang sudah ditutup. Saya coba bernegosiasi dengan pihak maskapai yang membolehkan saya naik tapi dengan syarat meninggalkan bagasi. Setelah berpikir cukup lama, saya memutuskan untuk menolak tawaran maskapai dan memilih untuk memesan penerbangan baru untuk keesokan harinya dari KL ke Bandung, dan membiarkan tiket penerbangan saat itu hangus. Malam itu, saya menginap di KLIA. Tidak banyak yang saya ceritakan di sini karena secara fisik dan psikis saya cukup kelelahan. Sebagian besar waktu saya gunakan untuk _browsing_, menonton film, mengetik _draft_ catatan perjalanan ini, dan mencoba beristirahat. Keesokan harinya, saya menaiki pesawat pukul 08:00 dan sampai dengan selamat di Bandung pukul 10:20 WIB.

Nah, demikian kisah perjalanan saya. Hal-hal yang bisa diambil sebagai pelajaran untuk kemudian hari dari pengalaman di atas adalah sbb:

- Jangan pernah ambil penerbangan yang jadwalnya mepet dengan penerbangan selanjutnya.
- Membeli tiket pulang pergi (p.p) dari dan ke Indonesia memang cukup mahal, namun lumayan menjamin supaya kita tidak terlantar di negeri orang jika terjadi sesuatu. Saya memutuskan untuk mengambil tiket p.p KL - AMS - KL karena harganya lebih murah, namun penerbangan p.p dari jakarta ke KL harus dipesan terpisah. Jika terlalu mepet, maka peristiwa di atas bisa terjadi pada anda dan pada akhirnya biaya yang dikeluarkan akan sama saja atau mungkin lebih mahal.
- Perbanyak minum air putih, supaya lebih fokus dalam perjalanan.
- Jangan takut bepergian meskipun sendiri, nikmati luasnya dunia, bertemulah dengan berbagai macam manusia. 

Sebagai penutup, saya kutip sebuah quote dari pengarang terkenal, J. R. R. Tolkien untuk anda semua sebagai penyemangat berpetualang.

  > not all who wander are lost <br>
  **(J. R. R Tolkien)**

Baca kembali:
- {{<raw>}}<a href="/posts/perjalanan-1/">Perjalanan (Part 1)</a>{{</raw>}}
- {{<raw>}}<a href="/posts/perjalanan-2/">Perjalanan (Part 2)</a>{{</raw>}} 

**[Kuala Lumpur, 15 Mei 2017; edited in Bandung, 16 Februari 2019]**
