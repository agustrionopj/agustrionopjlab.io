---
title: "My Cover: One Man's Dream (Yanni)"
date: 2019-05-29T23:52:02+07:00
description: "Rekaman cover lagu 'One Man's Dream (Yanni)'"
categories: ["Music"]
dropCap: false
displayInMenu: false
displayInList: true
draft: false

resources:
    - name: featuredImage
      src: "piano.jpg"
---

---
Enjoy!

#### Yanni - One Man's Dream

{{<raw>}}
<br>
{{<youtube "0hSBmu8rBnU">}}
{{</raw>}}
