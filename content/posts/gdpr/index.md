---
title: "Mengenal Lebih Jauh Tentang GDPR"
date: 2020-02-28T00:04:29+07:00
description: "Penjelasan singkat mengenai GDPR dan penerapannya dalam website yang kita buat."
categories: ["Learn"]
toc: false
dropCap: true
displayInMenu: false
displayInList: true
draft: false
resources:
- name: featuredImage
  src: "gdpr.png"
  params:
    description: ""
---

Pernahkah kalian saat berkunjung ke sebuah website kemudian disuguhi dengan tampilan *window/banner* yang berisi informasi tentang *cookies*? saya yakin pernah, dan bahkan sering. Kalian juga menemui *window/banner* itu saat pertama kalinya berkunjung ke website ini kan? *window/banner* inilah yang disebut sebagai *Cookie Consent* yang ada untuk memenuhi aturan GDPR. 

### GDPR dan sejarahnya
<!-- 
{{<raw>}}
<figure>
  <img src="1/contoh.png">
  <figcaption>Contoh GDPR (pojok kanan bawah) pada website <a href="https://bosscha.itb.ac.id" target="_blank">Observatorium Bosscha</a></figcaption>
</figure>
{{</raw>}} -->

![](1/contoh.png)
{{<raw>}}<p style="text-align:center">Contoh <i>Cookie Consent</i> pada website Observatorium Bosscha</p>{{</raw>}}

GDPR (*General Data Protection Regulation*) yang termanifestasi dalam bentuk *Cookie Consent* atau *Privacy Policy*, singkatnya berisi aturan-aturan mengenai cara meng-*handle* data konsumen (dalam hal ini bisa berupa data pengunjung atau *user* sebuah website). GDPR ini bertujuan meningkatkan perlindungan data konsumen sehingga menghindari penyalahgunaan data tanpa sepengetahuan (atau *consent*) konsumen itu sendiri.  Dengan demikian, GDPR mensyaratkan sebuah website atau organisasi untuk lebih menginformasikan kepada *user* tentang informasi apa saja yang dikumpulkan, dan bagaimana informasi itu digunakan serta memberikan kontrol lebih kepada *user* terhadap tindakan tersebut.

GDPR ini diinisiasi oleh Uni Eropa (EU) sekitar tahun 2016 dan mulai diberlakukan paksa (*enforceable*) semenjak tanggal 25 Mei 2018. Meskipun aturan ini dari EU (dan digunakan untuk melindungi konsumen EU), aturan ini tetap berlaku bagi siapapun yang memiliki bisnis atau website yang bisa diakses secara daring, karena secara teknis konsumen EU bisa mengakses bisnis atau website tersebut. 

![](1/gdpr-chart.png)

### Bagaimana jika website kita tidak memenuhi aturan dalam GDPR?

Penalti berupa denda akan dibebankan kepada website yang tidak memenuhi aturan GDPR. Besarnya denda bergantung pada besarnya pelanggaran yang ada &euro; 10 juta atau 2% pemasukan tahunan pelanggar untuk pelanggaran kecil; &euro; 20 juta atau 4% pemasukan pelanggar untuk pelanggaran besar --- nilai yang diambil adalah yang terbesar dari masing-masing kategori). Pelanggaran besar misalnya penyalahgunaan data, prosedur *data handling* tidak dilakukan, transfer data tanpa ijin, atau mengabaikan permintaan *user* untuk akses data. Pelanggaran yang lebih kecil seperti gagal melaporkan adanya pembobolan data, tidak memberi tahu *user* perihal pencurian data, atau gagal menerapkan prosedur perlindungan data.

### Bagaimana cara menambahkan GDPR ke website kita?

Ada beberapa penyedia layanan *Cookie Consent* gratis yang bisa digunakan. Salah satu yang menurut saya mudah digunakan adalah *Cookie consent* oleh {{<raw>}}<a href="https://www.osano.com/cookieconsent" target="_blank">Osano</a>{{</raw>}}. Berikut adalah langkah-langkahnya.

- Pada website {{<raw>}}<a href="https://www.osano.com/cookieconsent" target="_blank">Osano</a>{{</raw>}}, silakan klik `Open Source > Download`. Scroll pada bagian bawah, dan tekan `Start Coding`. Sebuah editor akan muncul beserta *preview* dari *banner Cookie Consent* kita.

{{<raw>}}
<p class="gallery" style="max-width: 1000px;">
  <a href="1/osano1.png" data-fancybox="images" >
    <img src="1/osano1.png" />
  </a>

  <a href="1/osano2.png" data-fancybox="images" >
    <img src="1/osano2.png" />
  </a>
</p>
{{</raw>}}

- Pilih posisi *Cookie Consent* yang diinginkan. *Preview* akan berubah mengikuti pilihan kita.
![](1/osano3.png)

- Pilih format tampilan dari *Cookie Consent* kita.
![](1/osano4.png)

- Pilih tema warna dari palet yang disediakan, atau buat sendiri.
![](1/osano5.png)

- Tambahkan tautan untuk detil lebih lanjut supaya *user* yang tertarik bisa membaca lebih detil tentang GDPR. Tautan ini bisa ke website luar atau laman yang dibuat sendiri.
![](1/osano6.png)

- Tambahkan tipe aturan yang diinginkan. Pilihannya antara lain: beritahu *user* bahwa kita menggunakan *cookies*, atau beri pilihan kepada *user* untuk menggunakan *cookies* atau tidak.
![](1/osano7.png)

- Beri/ubah tulisan pesan seperti yang diinginkan, misal diubah dalam bahasa Indonesia.
![](1/osano8.png)

- *Banner Cookie Consent* sudah siap digunakan. *Copy* masing-masing potongan kode dan letakkan sesuai dengan petunjuk (sebelum *tag* `</head>` dan `</body>` pada file `html` website kalian). Jika kalian menggunakan tema tertentu, sesuaikan peletakan kode ini dengan tema yang digunakan.
![](1/osano9.png)

Mudah bukan? Nah, bagi teman-teman yang akan/sedang membuat website, baik website pribadi maupun website untuk institusi/organisasi, jangan lupa tambahkan *Cookie Consent* ini untuk memenuhi aturan GDPR serta melindungi hak pengunjung website kita.

**[Observatorium Bosscha, 07:37 WIB]**

---
### Referensi:
- {{<raw>}}
<a href="https://www.ethos-marketing.com/blog/the-laymans-guide-to-gdpr/" target="_blank">https://www.ethos-marketing.com/blog/the-laymans-guide-to-gdpr/</a>{{</raw>}}
- {{<raw>}}<a href="https://www.coredna.com/blogs/general-data-protection-regulation" target="_blank">https://www.coredna.com/blogs/general-data-protection-regulation</a>{{</raw>}}
- {{<raw>}}<a href="https://www.internetsociety.org/blog/2018/05/gdpr-going-beyond-borders/?gclid=CjwKCAiA7t3yBRADEiwA4GFlI8QcJiTXSGl_FU_2sf_9_5bD3DfGTQKeqDaDsbxJoxAqphV-Kb7drxoCugwQAvD_BwE" target="_blank">https://www.internetsociety.org/blog/2018/05/gdpr-going-beyond-borders/?gclid=CjwKCAiA7t3yBRADEiwA4GFlI8QcJiTXSGl_FU_2sf_9_5bD3DfGTQKeqDaDsbxJoxAqphV-Kb7drxoCugwQAvD_BwE</a>{{</raw>}}
