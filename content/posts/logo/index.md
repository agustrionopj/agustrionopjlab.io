---
title: "Transformasi logo PJ's Eyes"
date: 2017-11-12T19:23:37+07:00
description: "Kisah singkat perancangan watermark yang saya gunakan pada foto-foto yang saya ambil."
categories: ["Learn"]
dropCap: True
displayInMenu: false
displayInList: true
draft: false

resources:
    - name: featuredImage
      src: "logo.jpg"
---

Semenjak awal saya belajar fotografi beberapa tahun silam, perancangan logo dengan mengusung _tagline_ **pj's eyes** telah dilakukan berulang kali. _Tagline_ ini sendiri punya makna khusus: **pj** adalah nama panggilan yang merupakan singkatan dari nama belakang saya; dan **eyes** bisa diartikan cara pandang saya ke dunia luar yang biasanya terekam dalam bentuk gambar diam/foto. Logo ini selalu saya pasang di setiap foto yang saya unggah ke media sosial, sebagai ciri khas, sekaligus tanda air (_watermark_). Sayang sekali, beberapa desain awal logo ini hilang bersamaan dengan rusaknya harddisk saya yang terdahulu. Kisah perjalanan logo ini cukup panjang, dari hanya sekedar tulisan, sampai dengan penambahan _icon_/gambar, Logo terakhir yang saya gunakan terlihat pada gambar berikut.

{{<smallimg src="1/watermark-old.png" alt="Logo lama" smartfloat="left" width="200px" clear="true">}}

Gambar dasar dari logo ini adalah kamera, yang dengan jelas menunjukkan bahwa tujuan utama logo ini adalah untuk dijadikan _watermark_ gambar-gambar yang saya ambil.

Nah, dua hari yang lalu, saya dimintai bantuan merancang logo sebuah toko online yang bergerak di penjualan buku dan pakaian. Saat melakukan hal ini, tiba-tiba terpikirkan oleh saya untuk memperbarui logo pj’s eyes supaya lebih segar. Setelah melakukan  _brain-storming_ dan _sketching_ selama dua hari ini, maka logo pj’s eyes yang baru adalah sebagai berikut.

{{<smallimg src="1/watermark-fin.png" alt="Logo lama" smartfloat="right" width="300px" clear="true">}}

_Icon_ pada logo melambangkan dua hal: galaksi (berkaitan dengan latar belakang pendidikan saya yaitu astronomi),  dan mata atau _eyes_, sesuai dengan _tagline_ yang saya gunakan.

_So, it is official now_

**[Bosscha Observatory, 06:47 WIB]**

##### PS:
Berikut adalah beberapa ide dan konsep saat perancangan logo ini.

{{<foldergallery src="2">}}

