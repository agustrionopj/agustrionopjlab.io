---
title: "Piano solo: My playing"
date: 2018-11-12T20:50:15+07:00
description: "Rekaman beberapa lagu yang saya mainkan"
categories: ["Music"]
dropCap: true
displayInMenu: false
displayInList: true
draft: false

resources:
    - name: featuredImage
      src: "piano.jpg"
---

Pada kesempatan kali ini saya akan berbagi beberapa lagu yang saya mainkan ulang. Selamat mendengarkan dan semoga terhibur ya :smile:.

PS: mohon dimaklumi kesalahan nada dan sebagainya, maklum masih belajar :smile:. Terima kasih.

#### Ludovico Einaudi - I giorni

{{<raw>}}
<br>
{{<youtube "fBleb85zsY4">}}
{{</raw>}}

#### Yann Tiersen - L'apres midi (OST Amelie)

{{<raw>}}
<br>
{{<youtube "40GGJA0HqVw">}}
{{</raw>}}

#### Ludovico Einaudi - Nuvole Bianche (white cloud)

{{<raw>}}
<br>
{{<youtube "HYugS0Xe8Uw">}}
{{</raw>}}