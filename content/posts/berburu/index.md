---
title: "Berburu"
date: 2017-04-25T13:04:51+07:00
description: "Kisah mencari tiket pesawat murah."
categories: ["Journey"]
dropCap: true
displayInMenu: false
displayInList: true
draft: false

resources:
- name: featuredImage
  src: "plane.jpg"
---

Sudah hampir seminggu ini saya berburu tiket pesawat Jakarta - Amsterdam demi mendapatkan harga terbaik. Nampak sekali harga-harga terkoreksi silih berganti, kadang murah, kadang mahal, dan hal itu terjadi dalam tempo yang cepat. Beberapa kali saya melewatkan jendela kesempatan terbaik, termasuk malam ini, saat _booking_ sudah dilakukan dan ternyata pembayaran via VCN ditolak. Selidik-selidik, ada beberapa situs luar negeri yang memblokir VCN dari Indonesia karena sering disalahgunakan. Saya jadi berpikir, betapa terkoneksinya satu manusia dengan manusia yang lain, bahkan tanpa kita sadari. Perbuatan buruk satu orang atau lebih, berimbas ke manusia yang lain,  disadari maupun tidak, diinginkan maupun tidak.

**[Bogor, 23.57 WIB, masih berburu ...]**

_Update, 28 April 2017: Alhamdulillah tiket sudah diperoleh_ :)
