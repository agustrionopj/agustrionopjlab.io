---
title: "ASTRO-Series YouTube Channel"
date: 2018-05-01T20:01:00+07:00
description: "Berisi dasar-dasar materi Astronomi"
categories: ["Learn"]
dropCap: true
displayInMenu: false
displayInList: true
draft: false

resources:
    - name: featuredImage
      src: "channel.jpg"
---

Hai,

Kali ini saya mencoba media lain dalam upaya menyebarkan ilmu Astronomi, terutama kepada teman-teman yang mungkin belum memiliki kesempatan belajar ataupun yang merasa kurang mendapatkan informasi Astronomi dalam bahasa Indonesia. Saat pembuatan rangkaian video ini, saya menganggap bahwa para _audience_ sudah mengenal konsep Matematika dasar dan Fisika dasar, sehingga durasi video yang saya unggah nantinya bisa dibatasi maksimum di kisaran 30 menit. _Feel free to distribute them_, selamat belajar, jangan lupa _like_ dan _subscribe_ yaa :smiley:

Rangkaian video ASTRO-Series bisa dilihat pada tautan berikut. Mohon masukannya juga ya :smile: . Terima kasih.

#### Episode 1: konsep dasar, intro sistem koordinat

{{<raw>}}
<br>
{{<youtube "i-E34Yhfs1U">}}
{{</raw>}}

#### Episode 2: aplikasi sistem koordinat

{{<raw>}}
<br>
{{<youtube "24EaSdpmbXE">}}
{{</raw>}}

#### Episode 3: sistem koordinat Horizon dan aplikasinya

{{<raw>}}
<br>
{{<youtube "gIfoGiCOmRo">}}
{{</raw>}}

#### Episode 4: sistem koordinat Equatorial I

{{<raw>}}
<br>
{{<youtube "CQbOUKX4v-Q">}}
{{</raw>}}

#### Episode 5: aplikasi sistem koordinat Equatorial I

{{<raw>}}
<br>
{{<youtube "-UkI1AVA-J4">}}
{{</raw>}}