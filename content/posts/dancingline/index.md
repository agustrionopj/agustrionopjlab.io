---
title: "My Cover: OST Dancing Line - The Piano"
date: 2020-06-18T13:37:08+07:00
description: "Saya mencoba memainkan ulang lagu tema game Dancing Line."
categories: ["Music"]
toc: false
dropCap: true
displayInMenu: false
displayInList: true
draft: false
resources:
- name: featuredImage
  src: "dancingline.jpg"
  params:
    description: ""
---

{{<raw>}}
<br>
{{<youtube "t9SJ2p3aTcQ">}}
{{</raw>}}