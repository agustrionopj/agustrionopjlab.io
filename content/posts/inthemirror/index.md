---
title: "My Cover: In the Mirror (Yanni)"
date: 2020-02-25T00:05:26+07:00
description: "Rekaman cover lagu 'In the Mirror (Yanni)' "
categories: ["Music"]
dropCap: false
displayInMenu: false
displayInList: true
draft: false

resources:
    - name: featuredImage
      src: "piano.jpg"
---

---
Finally, posting this after a while. Enjoy :)


#### Yanni - In the Mirror

{{<raw>}}
<br>
{{<youtube "2QXroR4yEwI">}}
{{</raw>}}
