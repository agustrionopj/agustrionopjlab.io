---
title: "Membuat Website Sendiri, Sulitkah?"
date: 2020-02-26T15:20:05+07:00
description: "Sebuah cerita tentang bagaimana saya awal mulanya belajar membuat dan mengurus konten web."
categories: ["Learn"]
dropCap: true
displayInMenu: false
displayInList: true
draft: false

resources:
    - name: featuredImage
      src: "language.png"
---

Berawal dari limpahan tugas mengurusi web {{<raw>}}<a href="https://bosscha.itb.ac.id" target="_blank">Observatorium Bosscha</a>{{</raw>}}, saya berkesempatan mempelajari hal baru yang belum pernah saya lakukan sebelumnya: membuat dan me-*manage* konten website.  Pada tahun 2017, dimana saat itu web Observatorium Bosscha menggunakan {{<raw>}}<a href="https://wordpress.com" target="_blank">WordPress (WP)</a>{{</raw>}} sebagai CMS-nya (CMS = *Content Management System*), alih tugas itu dilakukan dari seorang rekan ke saya.`WP` digunakan dengan alasan mudah dipelajari dan menjanjikan bahwa mengisi/memperbarui konten itu 'semudah menjentikkan jari'. 

Selama beberapa saat, saya mempelajari bagaimana cara menggunakan `WP` dengan langsung terjun mengisi konten web dan membuat website pribadi saya sendiri (gambar di bawah), iseng-iseng ceritanya :smile:. Nah, dari sinilah saya merasa bahwa lama kelamaan website Bosscha semakin berat untuk di-*deploy* dan diakses, seiring banyaknya konten yang diunggah. Selain itu, saya merasa kurang leluasa dalam bereksperimen dan mengakomodasi beberapa masukan baik dari user maupun rekan-rekan yang lain terkait fitur-fitur dalam website tersebut. `WP` mengandalkan plugin --- yang seringkali tidak gratis --- untuk menambahkan fitur ataupun men-*custom* fitur sesuai keperluan kita sehingga pada awal tahun 2019 saya memutuskan untuk mencari CMS baru pengganti `WP` ({{<raw>}}\(\ldots\){{</raw>}} dan itu artinya saya harus belajar lagi dari nol! :sweat:). 


{{<raw>}}
<p class="gallery" style="max-width: 1000px;">
  <a href="1/web-lama.png" data-fancybox="images" data-caption="Tampilan website saya di WordPress">
    <img src="1/web-lama.png" />
  </a>

<!-- 
  <a href="" data-fancybox="images" data-caption="Suasana kabin Turkish Airline">
    <img src="" />
  </a> -->
</p>
{{</raw>}}

Setelah melalui proses *browsing* dan baca-baca di internet, saya semakin bingung dengan jargon-jargon dalam dunia *web development* dan banyaknya pilihan *tools* untuk membuat dan mengembangkan sebuah website: `HTML`, `PHP`, `SQL`, `Apache`, `CSS`, `JavaScript`, `Go`/`Hugo`, dsb --- masing-masing dengan kelebihan dan kekurangannya. Hal ini sempat membuat saya *down* dan berpikir ulang untuk mengganti CMS yang sudah ada.   

Nah, saat proses mencari-cari itulah, saya menemukan beberapa artikel menarik terkait *Static Website* vs *Dynamic Website*. Secara umum, *Static Web* memiliki konten yang relatif tidak berubah setiap kali user berkunjung ke web tersebut, dan sebaliknya *Dynamic Website* kontennya berubah setiap kali pengunjung pergi ke website tersebut (Facebook adalah contoh yang baik dari sebuah *Dynamic WebSite*). Pembeda yang lain adalah *Dynamic Website* biasanya menggunakan *tools* seperti `JavaScript` dan `SQL` *database* di dalamnya. Di beberapa artikel yang lain juga disebutkan keunggulan dari *Static Website*, beberapa di antaranya adalah kecepatan dan keamanan, yang membuat saya lebih condong untuk memilih mengembangkan *Static Website* dibandingkan *Dynamic Website*. Tentu saja karena *Static Website* lebih cocok untuk keperluan web Observatorium Bosscha serta web pribadi saya.

Saya kemudian mencari-cari bahasa pemrograman yang cocok untuk keperluan ini, dan pilihan saya mengerucut kepada {{<raw>}}<a href="https://golang.org/" target="_blank">Go language (Golang)</a>{{</raw>}} dan turunannya: {{<raw>}}<a href="https://gohugo.io/" target="_blank">Hugo</a>{{</raw>}}.

Maka, dimulailah petualangan saya di dunia *web development* hingga saat ini :smiley:. 

Hasilnya? website {{<raw>}}<a href="https://bosscha.itb.ac.id" target="_blank">Observatorium Bosscha</a>{{</raw>}} dan website pribadi saya pun terlahir kembali. Saya sempat juga mengerjakan beberapa website *subdomain* dari Observatorium Bosscha untuk beberapa keperluan, seperti *event* {{<raw>}}<a href="https://bosscha.itb.ac.id/onthemoonagain" target="_blank">onthemoonagain</a>{{</raw>}} dan {{<raw>}}<a href="https://bosscha.itb.ac.id/gmc2019" target="_blank">Gerhana Matahari Cincin 2019</a>{{</raw>}}.

Saya akhirnya juga menyadari bahwa *tools* lain (selain Golang dan Hugo) juga diperlukan untuk lebih mengembangkan fitur dari website kita. Di beberapa kesempatan, saya harus mengutilisasi `HTML`, `JavaScript`, `CSS`, dan `SQL` (bergantung dari keperluan saat itu) dan menggabungkannya dengan `Go`/`Hugo` sebagai bahasa pemrograman utama.

Ternyata, membuat web itu menyenangkan juga ya hehehe :joy:

Di tulisan berikutnya nanti, saya akan perlahan memberikan *insight* perihal `Go`/`Hugo`. Semoga dilancarkan semesta.

**[Observatorium Bosscha, 15:20 WIB]**  
